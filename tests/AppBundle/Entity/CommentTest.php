<?php

namespace Tests\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;

/**
 * Class CommentTest
 */
class CommentTest extends AbstractEntityTest
{
    /**
     * @return array
     */
    public function entityPropertyProvider()
    {
        $mockArticle = $this->getMockBuilder(Article::class)->getMock();
        $mockComment = $this->getMockBuilder(Comment::class)->getMock();
        $mockCommentTwo = $this->getMockBuilder(Comment::class)->getMock();

        return [
            ['id', 'integer', 10],
            ['firstname', 'string', 'Jane'],
            ['lastname', 'string', 'Doe'],
            ['content', 'string', 'First comment'],
            ['createdAt', 'datetime', new \DateTime()],
            ['article', 'object', $mockArticle],
            ['comment', 'object', $mockComment],
            ['comments', 'collection', new ArrayCollection([$mockCommentTwo])],
        ];
    }

    /**
     * Get instance of entity to test.
     *
     * @return object
     */
    protected function getEntityInstance()
    {
        return new Comment();
    }
}
