<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ArticleTest
 */
class ArticleTest extends AbstractEntityTest
{

    /**
     * @return array
     */
    public function entityPropertyProvider()
    {
        $mockComment = $this->getMockBuilder(Comment::class)->getMock();

        return [
            ['id', 'integer', 10],
            ['createdAt', 'datetime', new \DateTime()],
            ['updateAt', 'datetime', new \DateTime()],
            ['title', 'string', 'First title'],
            ['content', 'string', 'First content'],
            ['comments', 'collection', new ArrayCollection([$mockComment])],
        ];
    }

    /**
     * Get instance of entity to test.
     *
     * @return object
     */
    protected function getEntityInstance()
    {
        return new Article();
    }
}
