<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController.
 *
 * @Security("has_role('ROLE_ADMIN')"))
 */
class AdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/admin", name="homepage_admin")
     *
     * @return Response
     */
    public function listAction(Request $request)
    {
        $articles = $this->get('app.article_manager')->getArticles();
        $comments = $this->get('app.comment_manager')->getComments();
        $form = $this->get('app.article_manager')->addArticle($request);

        return $this->render('admin/admin.html.twig', [
            'articles' => $articles,
            'comments' => $comments,
            'form' => $form,
        ]);
    }

    /**
     * @param int $articleId
     *
     * @Route("/admin/article/delete/{articleId}", name="delete_article")
     *
     * @return RedirectResponse
     */
    public function removeArticleAction($articleId)
    {
        $this->get('app.article_manager')->removeArticle($articleId);
    }

    /**
     * @param Request $request
     * @param int     $articleId
     *
     * @Route("/admin/article/{articleId}/edit", name="update_article", requirements={"articleId": "\d+"})
     *
     * @return Response
     */
    public function updateArticleAction(Request $request, $articleId)
    {
        $form = $this->get('app.article_manager')->updateArticle($request, $articleId);

        return $this->render('admin/update_article.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @param int $commentId
     *
     * @Route("/admin/comment/{commentId}", name="remove_comment")
     *
     * @return RedirectResponse
     */
    public function removeCommentAction($commentId)
    {
        return $this->get('app.comment_manager')->removeComment($commentId);
    }

    /**
     * @param int $commentId
     *
     * @Route("/admin/comment/authorize/{commentId}", name="authorize_comment")
     *
     * @return RedirectResponse
     */
    public function authorizeCommentAction($commentId)
    {
        $this->get('app.comment_manager')->authorizeComment($commentId);
    }
}
