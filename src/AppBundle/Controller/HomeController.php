<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function listArticlesAction()
    {
        $pagination = $this->get('app.article_manager')->getArticlesPaginated($this->getParameter('knp_page'));

        return $this->render('default/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Request $request
     * @param int     $articleId
     *
     * @Route("/{articleId}", name="one_article", requirements={"articleId": "\d+"})
     *
     * @return Response
     */
    public function oneArticleAction(Request $request, $articleId)
    {
        $article = $this->get('app.article_manager')->getArticle($articleId);
        $comments = $this->get('app.comment_manager')->getCommentsByArticle($articleId);
        $form = $this->get('app.comment_manager')->addComment($request, $articleId);

        return $this->render('default/single.html.twig', [
            'article' => $article,
            'comments' => $comments,
            'form' => $form,
            'author' => Article::AUTHOR,
        ]);
    }

    /**
     * @param Request $request
     * @param int     $commentId
     *
     * @Route("/comment/{commentId}", name="add_comment_to_comment", requirements={"commentId": "\d+"})
     *
     * @return Response
     */
    public function addCommentAction(Request $request, $commentId)
    {
        $form = $this->get('app.comment_manager')->addCommentToComment($request, $commentId);

        return $this->render('default/add_comment.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @param string $articleId
     * @param string $commentId
     *
     * @Route("/{articleId}/comment/notify/{commentId}", name="notify_comment")
     */
    public function notifyCommentAction($articleId, $commentId)
    {
        $this->get('app.comment_manager')->notifyComment($commentId, $articleId);
    }
}
