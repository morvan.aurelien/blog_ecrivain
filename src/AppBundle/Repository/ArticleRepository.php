<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class ArticleRepository.
 */
class ArticleRepository extends EntityRepository
{
    /**
     * @return Query
     */
    public function findArticles()
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a')
            ->orderBy('a.createdAt', 'DESC')
            ->getQuery();

        return $qb;
    }
}
