<?php

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * Class ContainsJsCode.
 *
 * @Annotation
 *
 * @author Aurélien Morvan <contact@aurelien-morvan.fr>
 */
class ContainsJsCode extends Constraint
{
    public $message = 'Insertion de code JS détecté';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
