<?php

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ContainsJsCodeValidator.
 *
 * @author Aurélien Morvan <contact@aurelien-morvan.fr>
 */
class ContainsJsCodeValidator extends ConstraintValidator
{
    /**
     * Check if user entry contains js code.
     *
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (preg_match('/<script[^>]*>(.*?)<\/script[^>]*>|<javascript[^>]*>(.*?)<\/javascript[^>]*>/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
