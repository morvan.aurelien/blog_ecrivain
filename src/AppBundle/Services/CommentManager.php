<?php

namespace AppBundle\Services;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use AppBundle\Form\Type\CommentType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CommentManager.
 */
class CommentManager
{
    /** @var EntityManager */
    private $entityManager;

    /** @var FormFactory */
    private $formFactory;

    /** @var TwigEngine */
    private $templating;

    /** @var ArticleManager */
    private $articleManager;

    /**
     * CommentManager constructor.
     *
     * @param EntityManager  $entityManager
     * @param FormFactory    $formFactory
     * @param TwigEngine     $templating
     * @param ArticleManager $articleManager
     */
    public function __construct(
        EntityManager $entityManager,
        FormFactory $formFactory,
        TwigEngine $templating,
        ArticleManager $articleManager
    ) {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->templating = $templating;
        $this->articleManager = $articleManager;
    }

    /**
     * @param Request $request
     * @param int     $articleId
     *
     * @return FormView
     *
     * @throws EntityNotFoundException
     */
    public function addComment(Request $request, int $articleId)
    {
        $article = $this->articleManager->getArticle($articleId);

        if (!$article instanceof Article) {
            throw new EntityNotFoundException(
                sprintf(
                    "The article with id '%d' not exist",
                    $articleId
                )
            );
        }

        $comment = new Comment();

        $form = $this->formFactory->create(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $comment->setArticle($article);

            $comment->setCreatedAt(new \DateTime());

            $this->entityManager->persist($comment);
            $this->entityManager->flush();

            $response = new RedirectResponse('/'.$articleId);

            $response->send();
        }

        return $form->createView();
    }

    /**
     * @param Request $request
     * @param int     $commentId
     *
     * @return FormView
     *
     * @throws EntityNotFoundException
     */
    public function addCommentToComment(Request $request, int $commentId)
    {
        $comment = $this->entityManager->getRepository(Comment::class)->find($commentId);

        if (!$comment instanceof Comment) {
            throw new EntityNotFoundException(
                sprintf(
                    "The comment with id '%d' not exist",
                    $commentId
                )
            );
        }

        $newComment = new Comment();

        $form = $this->formFactory->create(CommentType::class, $newComment);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $newComment->setCreatedAt(new \DateTime());
            $newComment->setComment($comment);

            $this->entityManager->persist($newComment);
            $this->entityManager->flush();

            $response = new RedirectResponse('/');

            $response->send();
        }

        return $form->createView();
    }

    /**
     * @param int $commentId
     *
     * @throws EntityNotFoundException
     */
    public function removeComment($commentId)
    {
        $comment = $this->entityManager->getRepository(Comment::class)->find($commentId);

        if (!$comment) {
            throw new EntityNotFoundException(
                sprintf(
                    "The comment with id '%d' not exist",
                    $commentId
                )
            );
        }

        if ($comment instanceof Comment) {
            $this->entityManager->remove($comment);
            $this->entityManager->flush();
        }

        $response = new RedirectResponse('/admin');
        $response->send();
    }

    /**
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->entityManager->getRepository(Comment::class)->findAll();
    }

    /**
     * @param int $articleId
     *
     * @return Comment[]|array
     */
    public function getCommentsByArticle(int $articleId)
    {
        $commentsArticle = $this->entityManager->getRepository(Comment::class)->findBy([
            'article' => $articleId,
        ]);

        return $commentsArticle;
    }

    /**
     * @param int $commentId
     * @param int $articleId
     *
     * @throws EntityNotFoundException
     */
    public function notifyComment(int $commentId, int $articleId)
    {
        $comment = $this->entityManager->getRepository(Comment::class)->find($commentId);

        if (!$comment instanceof Comment) {
            throw new EntityNotFoundException(
                sprintf(
                    "Comment with id '%d' not found",
                    $commentId
                )
            );
        }

        $comment->setReport(true);
        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        $response = new RedirectResponse('/'.$articleId);

        $response->send();
    }

    /**
     * @param int $commentId
     *
     * @throws EntityNotFoundException
     */
    public function authorizeComment(int $commentId)
    {
        $comment = $this->entityManager->getRepository(Comment::class)->findOneBy([
            'id' => $commentId,
        ]);

        if (!$comment instanceof Comment) {
            throw new EntityNotFoundException(
                sprintf(
                    "Comment with id '%d' not found",
                    $commentId
                )
            );
        }

        $comment->setReport(false);
        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        $response = new RedirectResponse('/admin');
        $response->send();
    }
}
