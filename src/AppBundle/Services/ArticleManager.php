<?php

namespace AppBundle\Services;

use AppBundle\Entity\Article;
use AppBundle\Form\Type\ArticleType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ArticleManager.
 */
class ArticleManager
{
    /** @var EntityManager */
    private $entityManager;

    /** @var FormFactory */
    private $formFactory;

    /** @var TwigEngine */
    private $twigEngine;

    /** @var Paginator */
    private $paginator;

    /** @var RequestStack */
    private $request;

    /**
     * ArticleManager constructor.
     *
     * @param EntityManager $entityManager
     * @param FormFactory   $formFactory
     * @param TwigEngine    $twigEngine
     * @param Paginator     $paginator
     * @param RequestStack  $request
     */
    public function __construct(
        EntityManager $entityManager,
        FormFactory $formFactory,
        TwigEngine $twigEngine,
        Paginator $paginator,
        RequestStack $request
    ) {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->twigEngine = $twigEngine;
        $this->paginator = $paginator;
        $this->request = $request;
    }

    /**
     * @return Article[]|array
     */
    public function getArticles()
    {
        return $this->entityManager->getRepository(Article::class)->findAll();
    }

    /**
     * @param int $articlePerPage
     *
     * @return PaginationInterface
     */
    public function getArticlesPaginated(int $articlePerPage)
    {
        $queryArticle = $this->entityManager->getRepository(Article::class)->findArticles();

        $pagination = $this->paginator->paginate(
            $queryArticle,
            $this->request->getCurrentRequest()->query->getInt('page', 1),
            $articlePerPage
        );

        return $pagination;
    }

    /**
     * @param int $articleId
     *
     * @return null|object
     */
    public function getArticle(int $articleId)
    {
        return $this->entityManager->getRepository(Article::class)
            ->findOneBy(
                [
                    'id' => $articleId,
                ]
            );
    }

    /**
     * @param int $articleId
     *
     * @throws EntityNotFoundException
     *
     * @return RedirectResponse
     */
    public function removeArticle(int $articleId)
    {
        $article = $this->entityManager->getRepository(Article::class)->find($articleId);
        try {
            if (!$article) {
                throw new EntityNotFoundException(
                    sprintf(
                        "The article with id '%d' not exist",
                        $articleId
                    )
                );
            }
        } catch (\Exception $exception) {
            $exception->getMessage();
        }

        if ($article instanceof Article) {
            $this->entityManager->remove($article);
            $this->entityManager->flush();
        }

        $response = new RedirectResponse('/admin');
        $response->send();
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\Form\FormView
     */
    public function addArticle(Request $request)
    {
        $article = new Article();

        $form = $this->formFactory->create(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setCreatedAt(new \DateTime());

            $this->entityManager->persist($article);
            $this->entityManager->flush();

            $response = new RedirectResponse('/'.$article->getId());

            $response->send();
        }

        return $form->createView();
    }

    /**
     * @param Request $request
     * @param int     $articleId
     *
     * @throws EntityNotFoundException
     *
     * @return FormView
     */
    public function updateArticle(Request $request, int $articleId)
    {
        $article = $this->entityManager->getRepository(Article::class)->find($articleId);

        try {
            if (!$article) {
                throw new EntityNotFoundException(
                    sprintf(
                        "The article with id '%d' not exist",
                        $articleId
                    )
                );
            }
        } catch (EntityNotFoundException $exception) {
            $exception->getMessage();
        }

        $form = $this->formFactory->create(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();

            $article->setUpdateAt(new \DateTime());
            $article->setTitle($datas->getTitle())
                ->setContent($datas->getContent());

            $this->entityManager->persist($article);
            $this->entityManager->flush();

            $response = new RedirectResponse('/admin');

            $response->send();
        }

        return $form->createView();
    }
}
