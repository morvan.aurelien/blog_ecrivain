CREATE DATABASE  IF NOT EXISTS `_blogecrivain` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `_blogecrivain`;
-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: localhost    Database: _blogecrivain
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `chapter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'1979-07-29 00:00:00',NULL,'Et optio.','Ut fuga non quibusdam itaque ut quia. Quia quibusdam commodi fugiat debitis voluptatum occaecati. Quam quaerat ipsum earum vel qui itaque voluptatem. Numquam dicta accusamus non laboriosam ad eius tempore error. Et autem sint laboriosam et.','1'),(2,'2007-09-30 00:00:00',NULL,'Adipisci vitae alias laudantium.','Enim excepturi quo dolor et officiis. Et consequatur earum aut consectetur cupiditate quae.\nIllo facilis accusamus ex voluptatem et. Nemo voluptatibus necessitatibus tenetur veniam odit ducimus consequuntur voluptatem. Quod pariatur distinctio quis suscipit.','2'),(3,'1974-06-10 00:00:00',NULL,'Non perspiciatis totam et.','Eius optio nihil voluptas et. Repellat repudiandae velit ex id sint quia velit nihil. Est et corrupti commodi ab. Quos perspiciatis tempora aut alias.','3'),(4,'2001-05-09 00:00:00',NULL,'Voluptatibus esse occaecati.','Modi quaerat aut veritatis impedit ut est. Et atque quam et impedit illum consequuntur consequatur reprehenderit. Et quae quod ea occaecati est. Dolorum omnis nostrum quia ut qui modi est.','4'),(5,'2000-02-19 00:00:00',NULL,'Animi ut et ut.','Et sit corrupti voluptas et ipsum. Fugit est vitae hic fuga neque enim nostrum. Perspiciatis expedita suscipit et et ex repudiandae.\nFugiat qui cumque quia iure quaerat delectus. Doloremque est dicta dolore velit impedit. Deserunt quia id voluptatem omnis magni.','5');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `report` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C7294869C` (`article_id`),
  KEY `IDX_9474526CF8697D13` (`comment_id`),
  CONSTRAINT `FK_9474526C7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FK_9474526CF8697D13` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,1,NULL,'Kurtis','Feest','Corrupti dolorem quisquam iusto magnam et. Porro et architecto inventore quasi. Id officiis harum laborum cumque molestiae nostrum.','1978-12-23 00:00:00',NULL),(2,1,NULL,'Fletcher','Christiansen','Voluptas et non assumenda id. Quis quis velit optio. Deserunt qui veniam eligendi enim asperiores. Saepe et natus voluptas quo qui earum.','2012-03-27 00:00:00',NULL),(3,1,NULL,'Mekhi','Schaden','Aut aut autem accusantium voluptatem reiciendis. Quam consequatur maxime velit molestiae. Eos iure dolor error odit.','1996-07-10 00:00:00',NULL),(4,2,NULL,'Elbert','Lesch','Sit iste magnam quidem nihil dignissimos. Facere aliquid doloremque corrupti quia non vel ut. Id cupiditate corrupti aut.','1985-03-22 00:00:00',NULL);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database '_blogecrivain'
--

--
-- Dumping routines for database '_blogecrivain'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-08 20:42:51
