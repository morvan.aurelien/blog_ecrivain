[![build status](https://gitlab.com/morvan.aurelien/blog_ecrivain/badges/_dev/build.svg)](https://gitlab.com/morvan.aurelien/blog_ecrivain/commits/_dev)
[![coverage report](https://gitlab.com/morvan.aurelien/blog_ecrivain/badges/_dev/coverage.svg)](https://gitlab.com/morvan.aurelien/blog_ecrivain/commits/_dev)
# Formation Openclassrooms - Projet 3 - Création blog pour l'écrivain Jean Forteroche
## Enoncé
Vous venez de décrocher un contrat avec Jean Forteroche, acteur et écrivain. Il travaille actuellement sur son prochain roman, "Billet simple pour l'Alaska". Il souhaite innover et le publier par épisode en ligne sur son propre site.

Seul problème : Jean n'aime pas WordPress et souhaite avoir son propre outil de blog, offrant des fonctionnalités plus simples. Vous allez donc devoir développer un moteur de blog en PHP et MySQL.

Vous développerez une application de blog simple en PHP et avec une base de données MySQL. Elle doit fournir une interface frontend (lecture des billets) et une interface backend (administration des billets pour l'écriture). On doit y retrouver tous les éléments d'un CRUD :
* Create : création de billets
* Read : lecture de billets
* Update : mise à jour de billets
* Delete : suppression de billets

Chaque billet doit permettre l'ajout de commentaires, qui pourront être modérés dans l'interface d'administration au besoin.  
Un commentaire peut être une réponse à un autre commentaire : dans ce cas, il est légèrement décalé vers la droite pour montrer l'arborescence des commentaires. Il ne peut pas y avoir plus de 3 sous-niveaux de commentaires.  
Les lecteurs doivent pouvoir "signaler" les commentaires pour que ceux-ci remontent plus facilement dans l'interface d'administration pour être modérés.

L'interface d'administration sera protégée par mot de passe. La rédaction de billets se fera dans une interface WYSIWYG basée sur TinyMCE, pour que Jean n'ait pas besoin de rédiger son histoire en HTML (on comprend qu'il n'ait pas très envie !).

Le code sera construit sur une architecture MVC. Vous développerez autant que possible en orienté objet. Au minimum, le modèle doit être construit sous forme d'objet.

**Ressources complémentaires**
En plus des cours du parcours, vous pouvez consulter les ressources suivantes pour vous aider :
* [eBook UML 2 par la pratique - Études de cas et exercices corrigés](https://openclassrooms.com/ebooks/uml-2-par-la-pratique-etudes-de-cas-et-exercices-corriges)
* [Chapitre "Fonctions d'aggrégation" du cours MySQL](https://openclassrooms.com/courses/administrez-vos-bases-de-donnees-avec-mysql/fonctions-d-agregation)

**Fichiers à fournir**
* Code HTML, CSS, PHP et JavaScript
* Export de la base de données MySQL

**Soutenance**
Pour cette soutenance, vous vous positionnerez comme un développeur présentant pendant 25 minutes son travail à son collègue plus senior dans l’agence web afin de vérifier que le projet peut être présenté tel quel à Jean Forteroche. Cette étape sera suivie de 5 minutes de questions/réponses.

## Présentation rapide de la solution adoptée
### Architecture application
Ce projet a été crée avec le framework Symfony.

Quand au serveur web hébergeant ce projet, il fonctionnera avec Apache 2.4 ainsi que sous PostgreSql pour des questions de rapidité d'execution.
Il sera possible également, par le biais de la configuration lors du composer install de choisir quel type de SGBD sera utilisée.

### Modèle de donnée
![Modèle de donnée](/docs/blog.png)

### Installation du projet

**1. Récupération du projet**

        git clone git@gitlab.com:morvan.aurelien/blog_ecrivain.git
        
**2. Install du projet**

        composer install
        
**3. Configuration projet**

- Renseigner les informations de database.
- Indiquer le nombre d'articles par page souhaité "knp_page"

**4. Création de la databse**

        php bin/console doctrine:database:create
        php bin/console doctrine:schema:create

**5. Chargement des jeux de données**

        php bin/console doctrine:fixtures:load
        
        
**6. Fichier SQL de la base**
[Database](/docs/museum.sql)
        

### Identificant administration

- Me demander en privé car non indiqué sur le dépôt :)